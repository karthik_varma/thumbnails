import image_func
import numpy as np
import gap_stat
from multiprocessing import Queue
from billiard import queues
import cv2
from image2pipe import *
from multiprocessing import queues
import os
import youtube_dl
import glob

from utils import logger_util

logger = logger_util.get_logger(__name__)



def yield_from_queue(q, timeout_sec=0.42):
    """
    Checks queue for new item and yields it
    If item is None then it is end of cycle

    :type timeout_sec: float
    :type q: queues.Queue
    """
    while True:
        try:
            x = q.get(True, timeout_sec)
            if x is None:
                break
            yield x
        except queues.Empty:
            pass


def download_video(url,dir):

    os.chdir(dir)

    ydl_opts = {}

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
         ydl.download([url])

    #logger.info('Downloaded video')

    list_of_files = glob.glob('./*')
    file = max(list_of_files, key=os.path.getctime)
    file = file[2:]

    logger.info("file name is: {}".format(file))

    v = cv2.VideoCapture(file)
    logger.info("capturing frames")

    count = v.get(cv2.CAP_PROP_FRAME_COUNT)
    fps_or = v.get(cv2.CAP_PROP_FPS)
    fps = v.get(cv2.CAP_PROP_FPS)
    if (count > 1000):
        print(count)
        sec = count // v.get(cv2.CAP_PROP_FPS)
        print(sec)
        print(fps)
        fps = 1000 // sec
    logger.info("fps: {}".format(fps))
    q = Queue()
    decoder = images_from_url(q, file, fps=fps, scale=(150, 150))
    logger.info("staring decoding")
    decoder.start()
    frames = []
    for pair in yield_from_queue(q):
        fn, img = pair
        frames.append(img)
    frames = np.array(frames)
    os.rename(str(file), 'a.mkv')
    return frames,fps_or,fps


def stillness(img_prev,img,img_next):
    diff = (np.sum(np.square(np.float32(img - img_prev))) + np.sum(np.square(np.float32(img_next - img))))
    if diff == 0:
        still = 1
    else:
        still = 1/diff
    return still

def frame_filter(frames,no_th):

#sending in all frames
    images =[]
    for i in range(len(frames)):
        image = frames[i]
        if (image_func.brightness(image) ==1) and (image_func.uniform(image) ==1) and (image_func.sharpness(image)==1) :
            images.append(i)
#removed low quality frames

    edg = []
    dil = []
    for i in range(len(images)):
        img = cv2.cvtColor(frames[images[i]], cv2.COLOR_BGR2GRAY)
        _,theta = cv2.threshold(img,127,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU) #try without threshold 127
        edg.append(cv2.Canny(theta,1,24,True))
        kernel = np.ones((6,6),np.uint8)
        dil.append(cv2.dilate(edg[i],kernel,iterations=1))
        edg[i] = np.concatenate([np.array(edg[i][x]) for x in range(150)])
        dil[i] = np.concatenate([np.array(dil[i][x]) for x in range(150)])
#create list of edge and
    s = []
    shot = []
    for i in range(len(images)-1):
#cal pixels in and pixels out ratio and removing the frames with transition
        numout = np.sum(np.multiply(edg[i]/255,dil[i+1]/255))
        numin = np.sum(np.multiply(dil[i]/255,edg[i+1]/255))
        dec = np.sum(edg[i]/255)
        pi = max((1 - (numout/dec)),(1 - (numin/dec)))

        if pi < 0.1:
            s.append(images[i])
        else:
            if len(s) >= 5:   #take only shots with length more than 5
                shot.append(s)
            s = []

    shot.append(s)
#removed all transition frames and made a 2dim array containing all shots

    vec_list = []
    vec_index = []

#calculating 2220dim vector of all images in shot
    for i in range(len(shot)):
        for j in range(len(shot[i])):
            image = frames[shot[i][j]]
            hsv = cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
            gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
            dx = cv2.Sobel(np.float32(gray), cv2.CV_32F, 1, 0, 3)
            dy = cv2.Sobel(np.float32(gray), cv2.CV_32F, 0, 1, 3)
            mag, ang = cv2.cartToPolar(dx, dy)
            vec = np.column_stack(([cv2.normalize(cv2.calcHist([hsv], [0], None, [128], [0, 255]),None)],
                                   [cv2.normalize(cv2.calcHist([hsv], [1], None, [128], [0, 255]),None)],
                                   [cv2.normalize(cv2.calcHist([hsv], [2], None, [128], [0, 255]),None)],
                                   [cv2.normalize(cv2.calcHist([mag], [0], None, [30], [0, 255]), None)],
                                   [cv2.normalize(cv2.calcHist([ang], [0], None, [30], [-3.14, 3.14]), None)]))
            for k in range(4):
                image = cv2.pyrDown(image)
                hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                dx = cv2.Sobel(np.float32(gray), cv2.CV_32F, 1, 0, 3)
                dy = cv2.Sobel(np.float32(gray), cv2.CV_32F, 0, 1, 3)
                mag, ang = cv2.cartToPolar(dx, dy)
                vec = np.column_stack((vec,[cv2.normalize(cv2.calcHist([hsv], [0], None, [128], [0, 255]), None)],
                                       [cv2.normalize(cv2.calcHist([hsv], [1], None, [128], [0, 255]), None)],
                                       [cv2.normalize(cv2.calcHist([hsv], [2], None, [128], [0, 255]), None)],
                                       [cv2.normalize(cv2.calcHist([mag], [0], None, [30], [0, 255]), None)],
                                       [cv2.normalize(cv2.calcHist([ang], [0], None, [30], [-3.14, 3.14]), None)]))

            vec_list.append(vec[0])
            vec_index.append(shot[i][j])
    vec_list = np.float32(vec_list)

#vector list of all images is converted to 32bit float and given as input for clustering, no.of clusters is equal to the no.of shots
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    ret, label, center = cv2.kmeans(vec_list, len(shot),None, criteria, 10,cv2.KMEANS_RANDOM_CENTERS)

#shots are divided further into subshots based on there label and one key frame is extracted per subshot based on stillness
    sub_shot = []
    sub = []
    k =0
    key_frames = []
    gif_cluster = []
    for i in range(len(shot)):
        for j in range(len(shot[i])-1):
            sub.append(shot[i][j])
            if label[k] != label[k+1]:
                sub_shot.append(sub)
                sub = []
            k += 1
        sub_shot.append(sub)
        sub = []
        k += 1
        for a in range(len(sub_shot)):
            still_max = 0
            for b in range(len(sub_shot[a])):
                if sub_shot[a][b] ==0:
                    still = 0
                else:
                    still = stillness(frames[sub_shot[a][b]-1],frames[sub_shot[a][b]],frames[sub_shot[a][b]+1])
                if still > still_max:
                    still_max = still
                    kf = sub_shot[a][b]
            key_frames.append(kf)
            gif_cluster.append([len(sub_shot[a]),sub_shot[a]])
        sub_shot = []
#cal the optimal no of clusters for the key frames using gap statistic method
    data = []
    data1 = []
    print(len(key_frames))
    print(key_frames)
    final_list = []
#if key frames are less than or equal to 5 directly give them as final thumbnails
    if len(key_frames)<=no_th:
        final_list = key_frames
        for i in range(len(no_th-key_frames)):
            final_list.append(key_frames[0])
    else:
        for i in range(len(key_frames)):
            ind = vec_index.index(key_frames[i])
            vec1 = vec_list[ind]
            vec_flat = np.zeros(2220,float)
            for j in range(2220):
                 vec_flat[j] = vec1[j][0]
            data.append(vec_flat)
            data1.append(vec1)
        data1 = np.float32(data1)
        data = np.array(data)
        max_clus = min(len(key_frames),11)
        min_clus = no_th
        k_opt = gap_stat.optimalK(data,10,min_clus,max_clus)
        print(k_opt)
#key frames are clustered into the K-optimal clusters
        ret, label, center = cv2.kmeans(data1, k_opt, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)

        clust_list = [[] for i in range(k_opt)]
        for i in range(len(key_frames)):
            clust_list[label[i][0]].append(key_frames[i])
        th_list = []
#take 1frame per cluster based and 
        for a in range(len(clust_list)):
            still_max2 = 0
            for b in range(len(clust_list[a])):
                still = stillness(frames[clust_list[a][b] - 1], frames[clust_list[a][b]], frames[clust_list[a][b] + 1])
                if still > still_max2:
                    still_max2 = still
                    th_frame = clust_list[a][b]
                th_size = len(clust_list[a])
            th_list.append([th_size,th_frame])
        th_list.sort(reverse=True)
        for i in range(len(th_list)):
            final_list.append(th_list[i][1])
    gif_len = 0
    for i in range(len(final_list)):
        gif_tuple =  gif_cluster[key_frames.index(final_list[i])]
        if gif_tuple[0] > gif_len:
            gif_len = gif_tuple[0]
            gif_final = gif_tuple[1]
    # for i in range(len(gif_final)):
    #     gif = frames[gif_final[i]]
    for i in range(len(final_list)):
        dim = (600, 600)
        img1 = cv2.resize(frames[final_list[i]], dim, interpolation=cv2.INTER_AREA)
        cv2.imwrite('f'+str(i) + '.jpg', img1)

    return final_list,gif_final


