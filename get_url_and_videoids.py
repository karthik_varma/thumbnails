from flask import jsonify
from utils.upload import UploaderService
from uuid import uuid4

from utils import logger_util

logger = logger_util.get_logger(__name__)

# TO GET THE URL AT WHICH THE FINAL THUMBNAILS ARE TO BE UPLOADED AND ALSO FOR RETURNING THE INITIAL DICT
# CONTAINING VIDEO IDs AND THE RESPECTIVE URLs OF THE SELECTED FRAMES(in an ordered manner)
def get_url_and_videoids(url1):

    url=[]
    videoids=[]
    number_of_thumbnails=[]

    for i in range(len(url1)):
        url.append(url1[i]['url'])

    for i in range(len(url1)):
        videoids.append(url1[i]['id'])

    for i in range(len(url1)):
        number_of_thumbnails.append(url1[i]['number_of_thumbnails'])

    # generating urls of output_thumbnails
    filenameOfThumbnail=[[] for i in range(len(videoids))]
    thumbnail_url = [[] for i in range(len(videoids))]
    gifpath=[]
    logger.info('creating directories for each url')

    # The for loop will generate URLs at which the selected frames will be uploaded
    for i in range(len(videoids)):
        uid=str(uuid4())                            # a unique id for each video_id
        for j in range(int(number_of_thumbnails[i])):    # Select the number of top frames as given input by the user
            filenameOfThumbnail[i].append(str(uid)+'_'+str(videoids[i])+'_'+str(j+1)+'.jpg')
            thumbnail_url[i].append(UploaderService.get_upload_url(filenameOfThumbnail[i][j]))
        gifpath.append(UploaderService.get_upload_url(str(uid)+'_'+str(videoids[i])+'.gif'))
    dict1=[]                                    # This dictionary will be displayed immediately when the hit is done
    logger.info("creating dict1 to be given as the first output")
    for i in range(len(videoids)):
        temp_dict = {"video_ids": videoids[i],"number of thumbnails": number_of_thumbnails[i],"url": thumbnail_url[i], "gifurl": gifpath[i]}
        dict1.append(temp_dict)

    logger.info(dict1)

    return jsonify(dict1),filenameOfThumbnail