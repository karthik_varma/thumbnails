#!/usr/bin/env python
# encode: utf-8


import logging
from multiprocessing import Queue
import billiard


from . import ffmpeg

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)



def images_from_url(q: Queue, video_url: str, ss: str = "00:00:00", fps: str = None, scale: tuple = (224, 224),
                    pix_fmt: str = "bgr24", vf: list = None):
    """

    :param ss: start second in a format of time "00:00:00"
    :param pix_fmt: rawcodec image format bgr24 or rgb24
    :type scale: tuple (width, height)
    :type fps: str
    :type video_url: str
    :type ss: str
    :type pix_fmt: str
    :type q: queues.Queue
    """

    ffmpeg_p = ffmpeg.images_from_url_subp(fps, scale, video_url, ss, image_format=pix_fmt, vf=vf)
    reader_p = billiard.context.Process(target=lambda: ffmpeg.enqueue_frames_from_output(ffmpeg_p, q, scale))
    reader_p.daemon = True
    return reader_p


