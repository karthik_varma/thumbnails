FROM gcr.io/google_appengine/python
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# ENV package="thumbnailr-service"

COPY $package/thumbnailr-service/storeo-dev-269851578927.json .
COPY thumbnailr-service/storeo-dev-269851578927.json .
ENV GOOGLE_APPLICATION_CREDENTIALS="storeo-dev-269851578927.json"

#RUN apt-get -y update

#RUN apt-get -y install software-properties-common

# Telegraf installation
RUN apt-get update

# For redis
RUN apt-get install -y redis-server

# Install dependencies
#COPY $package/requirements.txt .
COPY thumbnailr-service/requirements.txt .
RUN pip3 install -r requirements.txt
#RUN apt-get install libsm6
#RUN apt-get install -y libxrender-dev
#RUN apt update && apt install -y libsm6 libxext6
RUN apt update && apt install -y libsm6 libxext6 libxrender-dev
RUN apt-get install ffmpeg -y

# Copying firebase credentials
# COPY $package/firebase-credentials.json .

# Adding tusk package
#ADD tusk ./tusk
#RUN pip3 install -e tusk

#COPY $package .
COPY thumbnailr-service/ .
#EXPOSE $PORT
#CMD gunicorn -t 500 -b :$PORT celery_example:app --log-file logfile.log
#CMD echo "hello" & cat







EXPOSE $PORT
CMD /usr/bin/redis-server & celery worker -A task_main & gunicorn -b :8080 celery_example:app 
#CMD echo "hello" & cat
##EXPOSE $PORT
##CMD /usr/bin/redis-server & celery worker -A task_main --logfile=celerylogs.log & gunicorn -b :$PORT main:app --log-file logfile.log
