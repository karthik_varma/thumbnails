from celery import Celery

app = Celery(include = ["services.tasks.generator"])
app.config_from_object("celeryconfig")