import cv2
import numpy as np

def brightness(image):
    b = image[:,:,0]
    g = image[:,:,1]
    r = image[:,:,2]
    I = (0.2126 * np.mean(b) + 0.7152* np.mean(g) + 0.0722 * np.mean(r))/255.0
    #print(I)
    if I > 0.1:
        return 1
    else:
        return 0

def sharpness(image):
    img = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    dx = cv2.Sobel(np.float32(img),cv2.CV_32F,1,0,3)
    dy = cv2.Sobel(np.float32(img),cv2.CV_32F,0,1,3)

    mag,arg = cv2.cartToPolar(dx,dy)
    s = np.mean(mag)
    #print(s)
    if s > 0.1:
        return 1
    else:
        return 0

def uniform(image):
    img = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    hist = cv2.calcHist([img],[0],None,[128],[0,255])
    h_nor = cv2.normalize(hist,None)
    h_sort = np.array(sorted(h_nor,reverse = True))
    val = 0.0
    for i in range(int(len(h_sort)* 0.05)):
        val += h_sort[i]
    # print(h_sort)
    #print(val)
    if val > 1:
        return 1
    else:
        return 0
#
# import youtube_dl
# import os
#
# os.chdir('down')
#
# link = 'https://www.youtube.com/watch?v=fOekbdB23jU'
# opts = ['-f 22',link]
# youtube_dl.main(opts)
#
# os.chdir('..')

#
#
#
# def yield_from_queue(q, timeout_sec=0.42):
#     """
#     Checks queue for new item and yields it
#     If item is None then it is end of cycle
#
#     :type timeout_sec: float
#     :type q: queues.Queue
#     """
#     while True:
#         try:
#             x = q.get(True, timeout_sec)
#             if x is None:
#                 break
#             yield x
#         except queues.Empty:
#             pass
#
#
# def download_video(url):
#     ydl_opts = {}
#     with youtube_dl.YoutubeDL(ydl_opts) as ydl:
#         ydl.download([url])
#         info_dict = ydl.extract_info(url, download=False)
#         video_url = info_dict.get("url", None)
#         video_id = info_dict.get("id", None)
#         video_title = info_dict.get('title', None)
#     file1 = video_title + '-' + video_id + '.mkv'
#     file2 = video_title + '-' + video_id + '.mp4'
#     file3 = video_title + '-' + video_id + '.avi'
#     file4 = video_title + '-' + video_id + '.webm'
#     file = ''
#     if os.path.isfile(file1):
#         file = file1
#     elif os.path.isfile(file2):
#         file = file2
#     elif os.path.isfile(file3):
#         file = file3
#     elif os.path.isfile(file4):
#         file = file4
#     v = cv2.VideoCapture(file)
#     count = v.get(cv2.CAP_PROP_FRAME_COUNT)
#     print(v.get(cv2.CAP_PROP_FPS))
#     sec = count / v.get(cv2.CAP_PROP_FPS)
#     fps = v.get(cv2.CAP_PROP_FPS)
#     if (count > 1000):
#         print(count)
#         sec = count // v.get(cv2.CAP_PROP_FPS)
#         print(sec)
#         fps = 1000 // sec
#     q = Queue()
#     decoder = image2pipe.images_from_url(q, file, fps=fps, scale=(150, 150))
#     decoder.start()
#     frames = []
#     for pair in yield_from_queue(q):
#         fn, img = pair
#         frames.append(img)
#     return frames
