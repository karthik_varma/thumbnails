from tusk.services import TaskService
from configobj import ConfigObj
import logging
import sys
import os

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
stream = logging.StreamHandler(sys.stdout)
logger.addHandler(stream)

config = ConfigObj("config.ini")
env_variable = os.environ.get("APP_ENV", "test")

task_service = TaskService(**config[env_variable]['task'])


def task_create(user_id, meta, progress=5):
    task = task_service.create(user_id, 'AudioGeneration', save=True,
                               meta=meta, progress=progress, message='Starting Audio generation')
    return task


def task_get(task_id):
    task = task_service.get(task_id)
    return task


def task_finish(task_id):
    logger.info("Marking task with id {} as finished : ".format(task_id))
    task_service.increment_progress(task_id, None, 100, message='Audio generated')


def task_failed(task_id):
    logger.error("Marking task with id {} as failed : ".format(task_id))
    task_service.finish(task_id, -1, "Failed", message="Failed")

