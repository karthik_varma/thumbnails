import os
import requests
import shutil
from utils import logger_util
logger = logger_util.get_logger(__name__)

def download_file(url):
    local_filename = url.split('/')[-1]
    r = requests.get(url, stream=True)
    with open(local_filename, 'wb') as f:
        shutil.copyfileobj(r.raw, f)

    return local_filename

def delete_files_and_directories(directories, files):
   """
   :param directories: array containing directories which needs to be deleted
   :param files: array containing full path of files which needs to be deleted
   :return: nothing
   """
   for each_directory in directories:
       if os.path.exists(each_directory):
           shutil.rmtree(each_directory)

   for each_file in files:
       if os.path.exists(each_file):
           os.remove(each_file)

def create_directory(directory):
    """
        :param directory: folder path which needs to be created if not already present
        :return: nothing
    """
    if not os.path.exists(directory):
        logger.info("Creating directory {}".format(directory))
        os.makedirs(directory)


def get_filename_from_url(url):
    """
    :param url: relative path or full url
    :return: file name
    """
    file_name = url[url.rfind("/") + 1:]
    logger.info("File name is {} from url {}".format(file_name, url))
    return file_name


def get_file_from_path(from_path, to_path):
    """
    :param from_path: url or local path from which file has to be downloaded or copied
    :param to_path: local path where file should be kept
    """
    if from_path.startswith("http://") or from_path.startswith("https://"):
        # download to to_path
        logger.info("Downloading asset")
        file_name = get_filename_from_url(from_path)
        to_path = os.path.join(to_path, file_name)
        r = requests.get(from_path, allow_redirects=True)
        open(to_path, 'wb').write(r.content)
    else:
        shutil.copy2(from_path, to_path)