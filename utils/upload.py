from google.cloud import storage
from configobj import ConfigObj
from utils import logger_util

import os
logger = logger_util.get_logger(__name__)


class UploaderService:

    @classmethod
    def __get_configs(cls):
        config = ConfigObj("config.ini")
        logger.info("Config is : {}".format(config))

        config = config[os.environ.get("APP_ENV", "test")]
        bucket_id = config["google-cloud-storage"]["bucket_id"]
        dir_path = config["google-cloud-storage"]["dir_path"]
        domain = config["google-cloud-storage"]["domain"]
        project = config["google-cloud-storage"]["project"]

        return bucket_id, dir_path, domain, project

    @classmethod
    def get_upload_url(cls, destination_file_name):
        """
        Given destination file name, returns the url to which file will be uploaded
        :param destination_file_name: destination file name
        :return: full path of the url where file will be uploaded
        """
        bucket_id, dir_path, domain, project = cls.__get_configs()

        # Getting path to upload file to
        blob_path = os.path.join(dir_path, destination_file_name)
        url_path = os.path.join(domain, bucket_id, blob_path)
        return url_path

    @classmethod
    def upload_to_google_cloud(cls, source_file_path, destination_file_name):
        """
        Given a source file path in local directory, copies file to google storage
        :param source_file_path:
        :param destination_file_name:
        :return:
        """
        logger.info("In upload to google cloud, current working directory is {}".format(os.getcwd()))

        bucket_id, dir_path, domain, project = cls.__get_configs()
        # Getting path to upload file to
        blob_path = os.path.join(dir_path, destination_file_name)

        cls.upload_to_google_cloud_helper(project, bucket_id, source_file_path, blob_path)

        url_path = os.path.join(domain, bucket_id, blob_path)
        logger.info("url is : {}".format(url_path))
        return url_path

    @classmethod
    def upload_to_google_cloud_helper(cls, project, bucket_id, source_file_path, destination_file_path):
        """
       Given storage information like project, bucket_id and destination path,
       copies file at local path source_file_path to google storage
       :param project:
       :param bucket_id:
       :param source_file_path:
       :param destination_file_path:
       :return:
       """
        storage_client = storage.Client(project=project)
        bucket = storage_client.get_bucket(bucket_id)

        # Getting path to upload to
        blob = bucket.blob(destination_file_path)

        # uploading source file
        blob.upload_from_filename(source_file_path)

        # make it public
        blob.make_public()