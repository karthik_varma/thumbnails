import shutil
import os

def delete_files_and_directories(directories, files):
   """
   :param directories: array containing directories which needs to be deleted
   :param files: array containing full path of files which needs to be deleted
   :return: nothing
   """
   for each_directory in directories:
       if os.path.exists(each_directory):
           shutil.rmtree(each_directory)

   for each_file in files:
       if os.path.exists(each_file):
           os.remove(each_file)