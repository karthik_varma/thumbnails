from celery import Celery
from utils import utils
from uuid import uuid4
from frame_filter import *
from utils.upload import UploaderService
from utils import logger_util

logger = logger_util.get_logger(__name__)

celery_app = Celery('task_main', broker='redis://localhost//')


@celery_app.task(name='celery_example.thumbnailr')
def get_thumbnail(filenameOfThumbnail, url1):
    url = []
    videoids = []
    number_of_thumbnails = []

    for i in range(len(url1)):
        url.append(url1[i]['url'])

    for i in range(len(url1)):
        videoids.append(url1[i]['id'])

    for i in range(len(url1)):
        number_of_thumbnails.append(int(url1[i]['number_of_thumbnails']))

    # Creating directories for each url
    temp_dir = 'temp_dir'
    directories = []
    utils.create_directory(temp_dir)

    for i in range(len(url)):
        uid = str(uuid4())
        each_temp_dir = os.path.join(temp_dir, videoids[i] + "_" + uid)
        directories.append(each_temp_dir)
        utils.create_directory(directories[i])
    logger.info('Entering frame filter')
    path = str(os.getcwd())
    # frame_filter code
    thumbnails_list = [[] for i in range(len(url))]
    gifpath = []
    output_thumbnails = [[] for i in range(len(url))]
    for j in range(len(url)):
        frames_list, fps_video, fps_frames = download_video(url[j],directories[j])
        logger.info('Completed Video to frames')

        thumbnails, gif = frame_filter(frames_list,number_of_thumbnails[j])
        for i in range(min(number_of_thumbnails[j], len(thumbnails))):
            fr_time = (int(((thumbnails[i] + 1) * fps_video) / fps_frames))
            logger.info('current dir {}'.format(os.getcwd()))
            os.system("ffmpeg -i a.mkv -vf \"select=eq(n\\," + str(fr_time) + ")\" -vframes 1 " +str(i+1)+".jpg -hide_banner")
        s = ''
        for i in range(len(gif)):
            s = s + "+eq(n\\," + str(gif[i]) + ")"
        s = s[1:]
        os.system("ffmpeg -i a.mkv -vf select=\"" + s + "\" "+str(videoids[j])+".gif -vsync 0 -hide_banner")
        os.chdir(path)
        for i in range(min(number_of_thumbnails[j],len(thumbnails))):
            local_path = directories[j]+'/'+str(i+1) + '.jpg'
            thumbnails_list[j].append('./'+local_path)
            output_thumbnails[j].append(filenameOfThumbnail[j][i])
        gifpath.append(directories[j]+'/'+str(videoids[j])+'.gif')
        logger.info('Finished generatin Thumbnails and gif')

    for a, each_video_thumbnails in enumerate(thumbnails_list):
        for b, each_thumbnail in enumerate(each_video_thumbnails):
            UploaderService.upload_to_google_cloud(each_thumbnail, output_thumbnails[a][b])
        UploaderService.upload_to_google_cloud(gifpath[a], output_thumbnails[a][0][:-6] + '.gif')

    logger.info('Finished uploading')
    utils.delete_files_and_directories(directories,[])
