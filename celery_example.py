from flask import Flask
from flask_celery import make_celery
from celeryconfig import CELERY_RESULT_BACKEND
from task_main import get_thumbnail
from get_url_and_videoids import get_url_and_videoids
from flask import request, jsonify
from utils import logger_util
logger = logger_util.get_logger(__name__)


app = Flask(__name__)
app.config['CELERY_BROKER_URL']= CELERY_RESULT_BACKEND

celery = make_celery(app)
## using celery here because app name is already used by the flask app

@app.route('/url', methods=['POST'])
def get_top_thumbnails():

    url1 = request.get_json()
    (dict1, filenameOfThumbnail) = get_url_and_videoids(url1)
    get_thumbnail.delay(filenameOfThumbnail,url1)                         # tasks.py
    logger.info("Thank you. Please wait while the images are being uploaded")
    logger.info("Type of dict : {}, dict is : {}".format(type(dict1), dict1))
    return dict1

if __name__ == "__main__":
    app.run(debug=True, port=5000)  # run app in debug mode on port 5000












